﻿using System;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;
using System.Threading.Tasks;

namespace PhotoElectricSensor
{
    public partial class Form1 : Form
    {
        int i;
        int a;

        delegate void SetTextCallback();
        
        public Form1()
        {
            InitializeComponent();
            

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            serialPort1.Open();
            serialPort1.PinChanged += new SerialPinChangedEventHandler(port_PinChanged);
            timer1.Enabled = true;
            timer2.Enabled = true;
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            a = listBox1.Items.Count;
            // MessageBox.Show(j.ToString(), "Timer1");
           
        }


        private async void timer2_TickAsync(object sender, EventArgs e)
        {
           
            //MessageBox.Show(j.ToString(), "Timer2");
            if (a == listBox1.Items.Count && a != 0)
            {

                i++;
                this.label1.Text = i.ToString();
                listBox1.Items.Clear();
                timer1.Enabled = false;
                timer2.Enabled = false;

                serialPort1.RtsEnable = true; //ไฟเขียวติด
                serialPort1.DtrEnable = true; //เสียงร้อง
                await Task.Delay(1000); // ดีเลย์แบบไม่ sleep
                //Thread.Sleep(2000);   //ดีเลย์แบบ sleep
                serialPort1.DtrEnable = false; //ปิดเสียง
                serialPort1.RtsEnable = false; //ไฟเขียวดับ

            }

        }

        public void port_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
           // MessageBox.Show(j.ToString(), "pinchange");
            if (e.EventType == SerialPinChange.CtsChanged)
            {
                SetText();
                
            }
        }
        private void SetText()
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
  
            if (this.label1.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d);
                
            }
            else
            {
               
                listBox1.Items.Add(SerialPinChange.CtsChanged);
                timer1.Enabled = true;
                timer2.Enabled = true;
              
            }
           
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            serialPort1.DtrEnable = true;
            serialPort1.RtsEnable = true;
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            serialPort1.DtrEnable = false;
            serialPort1.RtsEnable = false;
        }

    }

}

